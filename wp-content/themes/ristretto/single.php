<?php get_header(); ?>
<div class="page-fade-top"></div>
	<?php if(have_posts()) : while(have_posts()) : the_post(); // start loop ?>

	<div class="inner">
		<div class="bio">
			<!--profile-->
			<article>
				<h1><?php the_title(); ?></h1>
				<?php the_content(); ?>
			</article>
		</div>
	</div>
		<?php endwhile; endif; // end loop ?>
		<!--profile photo-->
		<div class="image">
			<?php
			 if ( has_post_thumbnail()) {
					$large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'large');
					the_post_thumbnail('full');
			 }
		 	?>
	 </div>
	<div class="inner">
		<!--previous/next-->
		<nav role="navigation" class="post-nav">
			<div class="previous-post">
				<?php next_post_link('%link', '<i class="fa fa-caret-left"></i> Previous')?>

			</div>
			<div class="next-post">
				<?php previous_post_link('%link', 'Next <i class="fa fa-caret-right"></i>'); ?>
			</div>
		</nav>
	</div>

	<?php //get_sidebar(); ?>

<?php get_footer(); ?>
