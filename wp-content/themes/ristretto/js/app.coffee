jQuery(document).ready ($) ->

  $(window).load ->
  #beacon zoom
  $(window).scroll ->
    if $(window).width() >= 540 #desktop
      scroll = $(window).scrollTop()
      $('.zoom img').css transform: 'translate3d(0%, -' + scroll / 900 + '%, 0) scale(' + (100 + scroll / 25 ) / 100 + ')'
    else if $(window).width() <= 540 #mobile
      scroll = $(window).scrollTop()
      $('.zoom img').css transform: 'translate3d(0%, -' + scroll / 500 + '%, 0) scale(' + (100 + scroll / 3 ) / 100 + ')'
      # body...
    return


  #fading tagline
  $(window).scroll ->
    if $(window).width() >= 540
      $('.tagline').css 'opacity', 1 - ($(window).scrollTop() / 0)
      offsetTop = $('.extratext').offset().top
      $('.tagline').css 'opacity', 1 - (($(window).scrollTop() - offsetTop) / 700)
    else if $(window).width() <= 540
      $('.tagline').css 'opacity', 1 - ($(window).scrollTop() / 1000)
      offsetTop = $('.extratext').offset().top
      $('.tagline').css 'opacity', 1 - (($(window).scrollTop() - offsetTop) / 700)
    return

  $(window).scroll ->

  ### Check the location of each desired element ###

  $('.page-fade-top').each (i) ->
    bottom_of_object = $(this).position().top + $(this).outerHeight()
    bottom_of_window = $(window).scrollTop() + $(window).height()

    ### If the object is completely visible in the window, fade it it ###

    if bottom_of_window > bottom_of_object
      $(this).animate { 'opacity': '1' }, 500
    return
  return
