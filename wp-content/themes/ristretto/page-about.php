<?php get_header(); ?>
	<?php if(have_posts()) : while(have_posts()) : the_post(); // start loop ?>


    <div class="inner">
      <div class="flex">
        <!--Map-->
        <div class="flex-row">
          <iframe src="https://snazzymaps.com/embed/62149" width="100%" height="100%" style="border:none;"></iframe>
        </div>
        <!--information-->
        <div class="flex-row">
          <div class="tag">
      			<h4 class="tagline">Shining light on the issues</br>that plague our society today.</h4>
      			<!--<h6 class="seniors">Seniors</h6>-->
            <p>The Visual Communication Design program at ASU is hosting their 26th Annual Graduation Exhibition. The Class of 2018 invites you to come together and learn the steps you can take to solve problems that impact us everyday.</p>
            <div class="flex">
              <div class="info">
                US Bank Center, 29th floor<br>
                101 N 1st Ave<br>
                Phoenix AZ 85003
              </div>
              <div class="info">
                Opening Night<br>
                6pm - 8pm, May 4<br><br>

                Open to public<br>
                10am - 4pm, May 4-6
              </div>
            </div>
          </div>
        </div>

    </div>

	<?php endwhile; endif; // end loop ?>

<?php get_footer(); ?>
