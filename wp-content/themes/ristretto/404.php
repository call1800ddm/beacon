<?php get_header(); ?>

<div class="inner">
	<h1>404: Page Not Found</h1>
	<p>Looks like the page you're looking for isn't here anymore.</p>
	<p><a href="<?php bloginfo('url'); ?>">Return home.</a></p>
</div>

<?php get_footer(); ?>