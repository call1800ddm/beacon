<?php get_header(); ?>

<!-- beacon zoom-->
<div class="zoom">
	<img src="img/Beacon_UPDATED.jpg" class="beacon" alt="img"/>
</div>

<div class="inner">
	<div class="page-fade-top"></div>
	<div class="profiles" data-scroll-speed="2">
		<div class="tag">
			<h2 class="tagline">Shining light on the issues</br>that plague our society today.</h2>
			<!--<h6 class="seniors">Seniors</h6>-->
		</div>
		<div class="name" id="posts">
			<?php if(have_posts()) : while(have_posts()) : the_post(); // start loop ?>
				<article>
					<!--classlist-->
					<h1 class="names">
						<a href="<?php the_permalink(); ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a>
					</h1>
					<?php //the_content(); ?>

				</article>

			<?php endwhile; endif; // end loop ?>
		</div>
		<nav role="navigation" class="post-nav">
			<div class="previous-post">
				<?php previous_posts_link('<i class="fa fa-caret-left"></i> Previous'); ?>
			</div>
			<div class="next-post">
				<?php next_posts_link('Next <i class="fa fa-caret-right"></i>');?>
			</div>
		</nav>
	</div>
	<?php //get_sidebar(); ?>

	<?php get_footer(); ?>
</div>
